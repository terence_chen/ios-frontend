/**
 * iOS Frontend/js/mobile-app.js`
 *
 * Worked on by: Silvery Fu (Team 1, Group A)
 *   2014-06-18: Created login.js, added basic login user interface
 *   2014-06-23: Added connection with the endpoint, met with the cross-domain ajax problem
 *   2014-06-24: Revised the PhoneGap whitelisting mechanism, ajax request for login can be sent
 *   2014-06-26: Merged the original logout.js into this file
 *   2014-06-28: Added support for ajax DELETE request, logout request can be sent
 *   2014-07-03: Added solution to csrf-token problem, both login and logout succeed
 *   2014-07-04: Added local storage acts as cookies; Refactored the code
 *   2014-07-14: Revised to use response json to set cookies, added profile page
 *   2014-07-16: Renamed login.js, login.css into to moblie-app.js and mobile-app.css
 *   2014-07-19: Added redirection button to the change profile page, which only appears when user logged in
 *   2014-07-20: Revised profile page, completed change password feature
 *   2014-07-21: Revised profile page, completed change user name feature
 *   2014-07-21: Added csrf-token as a configuration
 *   2014-07-21: Added header bar and user name display
 */

$(function() {
  var doc$ = $(document)
    , form$ = $('#mform')
    , form_pwd$ = $('#mform_pwd')
    , form_name$ = $('#mform_name')
    , logout_bt$ = $('#mbt_logout')
    , profile_div$ = $('#profile_div')
    , course_bt$ = $('#mbt_course')
    , logo$ = $('#mlogo')
    , heading$ = $('h1')
    , new_name$ = $('#new_name')
    , url_base = 'http://candyman.biz';
  setCookie('url_base', url_base);
  
  /* Store the cookies as key-value pair in the local storage */
  function storeCookie(cookies) {
    clearCookie();
    $.each(cookies, function(key, val) {
      setCookie(key, val);
      $.cookie(key, val);
    });
  }

  function getCookie(co_name) {
    $.cookie(co_name);
    return localStorage.getItem(co_name);
  }
  
  function setCookie(co_key, co_value) {
    $.cookie(co_key, co_value);
    return localStorage.setItem(co_key, co_value);
  }

  function removeCookie(co_key) {
    delete window.localStorage[co_key];
  }

  function clearCookie() {
    return localStorage.clear();
  }
  
  /* Change to a target page */
  function change_to_page(target) {
    window.location.href = target;
  };

  course_bt$.click(function(){
    change_to_page('./src/course.html');
  });
  
  window.Accounts = {
    getCookie:getCookie,
    setCookie:setCookie,
    clearCookie:clearCookie,
    removeCookie:removeCookie,
    getCourses: function() {
      var url_base = this.getCookie('url_base')
        , endpoint = url_base + '/api/accounts/courses';
  
      return $.when($.get(endpoint, function(data) {
        console.log(data);
        return data;
      }));
    }
  };

  /* Change the login page according to flag */
  function login_page_transit(flag) {
    if(flag == 0) {
      profile_div$.removeClass('hide');
      form$.addClass('hide');
      logo$.addClass('hide');
      console.log(getCookie('name'));
      heading$.text(getCookie('name'));
    } else {
      profile_div$.addClass('hide');
      form$.removeClass('hide');
      logo$.removeClass('hide');
      heading$.text('');
    }
  }
  
  /* Event on login, logout */
  doc$.on('$login', function(event, data) {
    console.log('Succesful login.');

    /* Store the response cookie into local storage */
    storeCookie($.parseJSON(data));

    
    /* Now the user can visit the change profile page, see the navbar */
    login_page_transit(0);
    
  }).on('$logout', function(event) {
    alert("You are now logged out.");
    
    /* Now the user cannot visit the change profile page */
    login_page_transit(1);

    clearCookie();
    
  }).on('$check_status', function(event) {
    if(getCookie('email') != null) {
      console.log('user status checked');
      login_page_transit(0);
      } else {
        login_page_transit(1);
      }
  });
  
  /* Check the login-status of the user */
  doc$.on("pageshow", function(){
    doc$.trigger('$check_status');
  });

  doc$.trigger('$check_status');

  /* Event on changing names */
  heading$.on('$add_name', function(event) {
    heading$.text(getCookie('name'));
  }).on('$remove_name', function() {
    heading$.text('');
  })
  
  $.ajaxSetup({
    /* Set the csrf-token in the request header */
    beforeSend: function(xhr) {
      xhr.setRequestHeader('X-CSRFToken', getCookie('csrftoken'));
    }
  });
  
  /* Send the ajax login request, submitting the credentials */
  form$.submit(function(event) {
    var form_data = form$.serialize();
    event.preventDefault();
    $.ajax({
      type: 'POST',
      url: url_base + '/api/accounts/session',
      dataType: 'text',
      data: form_data,
    
      /* Trigger the login event */
      success: function(data, textStatus, request) {
        doc$.trigger('$login', data);
        heading$.trigger('$add_name');
      },
      
      /* Clear the cookie if the previous cookie is expired */
      statusCode: {
        403: function() {
          clearCookie();
        }
      }
    });
  });
  
  /* Send the ajax logout request, clear the local storage */
  logout_bt$.click(function() {
    $.ajax({
      type: 'DELETE',
      url: url_base + '/api/accounts/session',
      dataType: 'text',
      
      /* Trigger the logout event */
      success: function(data, textStatus, request) {
        doc$.trigger('$logout');
        heading$.trigger('$remove_name');
      },
      
      error: function(xhr, status, error) {
        login_page_transit(1);
      },
    });
  });

  /* Commit the change password transaction */
  form_pwd$.submit(function(event) {
    var form_data = form_pwd$.serialize();
    event.preventDefault();
    $.ajax({
      type: 'PATCH',
      url: url_base + '/api/accounts/session',
      dataType: 'text',
      data: form_data,
      
      success: function(data, textStatus, request) {
        console.log('Succesfully Change Password.');
      },
    });
  });
  
  /* Commit the change user name transaction */
  form_name$.submit(function(event) {
    var form_data = form_name$.serialize();
    event.preventDefault();
    $.ajax({
      type: 'PATCH',
      url: url_base + '/api/accounts/session',
      dataType: 'text',
      data: form_data,
      
      success: function(data, textStatus, request) {
        console.log('Succesfully Change User Name.');
        setCookie('name', new_name$.val());
        heading$.trigger('$add_name');
      },
    });
  });
});
