/*
File: attendanceHistory.js
Author: Shawn Dhawan
Description: App Logic for attendance history functionality
Date Last Modified: July 22, 2014
*/

$(document).ready(function() { 
    /*on load run this function*/
    var class_id = localStorage.getItem('current_course_id');
    var student_id = localStorage.getItem('id');
    var times_absent = 0;
    var times_present = 0
      , url_base = "http://candyman.biz";
    
    $(function() {
        drawTableContent()
        checkAttendanceHistory();
    });
    
    /*For Table Refresh Button*/
    $('#refreshAttendanceHistory').off("click", function() {
        removeTableContent();
        checkAttendanceHistory();
    });
    
    $('#attendanceHistoryDisplayOption').change(function() {
        var display_value = $('#attendanceHistoryDisplayOption input:checked').val();
        if(display_value == 'SHOW_ALL')
        {
            $('.attendancePresent').show()
            $('.attendanceAbsent').show()
        }
        else if(display_value == 'SHOW_PRESENT')
        {
            $('.attendancePresent').show()
            $('.attendanceAbsent').hide()
        }
        else if(display_value == 'SHOW_ABSENT')
        {
            $('.attendancePresent').hide()
            $('.attendanceAbsent').show()
        }
    });
    
    /*Will hide the table and display that there is no history available*/
    function hideTable() {
        $('#attendanceHistoryTable').hide();
        if($('.noHistoryData').length == 0) {
            $('#attendanceHistoryTable').after('<p class="noHistoryData">No history yet recorded.</p>');
        }
        $('#attendanceHistoryDisplayOption').hide();
    }
    
    /*Will unhide the table, and remove any "No History Data" identifers*/
    function showTable() {
        $('#attendanceHistoryTable').show();
        $('.noHistoryData').remove()
        $('#attendanceHistoryDisplayOption').show();
    }
    
    /*Will remove all <tr>'s under the heading.*/
    function removeTableContent() {
        $('#attendanceHistoryTable').find("tr:gt(0)").remove();
    }
    
    /*Will insert <tr>'s for each entry into the table.*/
    function drawTableContent() {
        times_present = 0;
        times_absent = 0;
        /*Gets data from local store*/
        data = JSON.parse(window.localStorage.getItem(class_id+"attendance_history_cache"+student_id));
        if(data == undefined) {
            console.log("No Data Cached");
            hideTable()
        }
        else if(data.length > 0) { //Got Data from server?
            showTable();
            for(var i = 0; i < data.length; i++) //add rows to table
            {
              table_row = $('<tr>');

              if((i % 2) == 1){
                table_row.addClass('style2');
              }

              var formatted_time_stamp = '';
              for(var j = 0; j < 10; j++) {
                formatted_time_stamp = formatted_time_stamp + data[i].timestamp[j];
              }

              table_row.append('<td>'+ formatted_time_stamp +'</td>'); //timestamp column -- still requires date formatting

              var student_status = data[i].status; //status column
              if(student_status == 1) {
                  table_row.append('<td>Present</td>');
                  table_row.addClass('attendancePresent');
                  times_present = times_present + 1;
              }
              else {
                  table_row.append('<td>Absent</td>');
                  table_row.addClass('attendanceAbsent');
                  times_absent = times_absent + 1;
              }

              $('#attendanceHistoryTable tbody').append(table_row);
            }
        }
        else { //no rows to display
            hideTable();
        }
        $('#attendancePresentButtonLabel').text("Present: " + times_present);
        $('#attendanceAbsentButtonLabel').text("Absent: " + times_absent);
    }/*End drawTableContent()*/
    
    /*Ajax Function to request data*/
    function checkAttendanceHistory() {
        $.ajax({                                      
              url: url_base + "/api/group_e/Attendance/?format=json" + "&student_id=" + student_id + "&class_id=" + class_id, 
              type: "GET",        
              data: "",
              dataType: 'json',
              success: function(data)
              {
                  /*Stores JSON to localStore*/
                  window.localStorage.setItem(class_id+"attendance_history_cache"+student_id, JSON.stringify(data));
                  console.log(JSON.stringify(data));
                  $('#AttendanceHistoryNoConnectivity').hide();
              },
            
              error: function(data) {
                  console.log("No Data Connectivity");
                  $('#AttendanceHistoryNoConnectivity').show();
              },
            
              complete: function() {
                  removeTableContent();
                  drawTableContent()
              }
        });
    }
});