/* group_d_material.js

Description: make ajax calls to populate all the class materials uploaded by the instructor

Team 1 Group D 

Created by: Jordan Tseng, 2014-07-05

Programmers: Jordan Tseng, Joyce Ling

Changes: 
2014-07-05  Fixed displaying content
2014-07-05  Fixed incorrect ajax method call
2014-07-05  Added a download button linking to the file
2014-07-05  Fixed alignments of columns
2014-07-05  Added file type and size info

Known bugs: 
None
*/

/*run function on load */
$(document).delegate('#course_page', 'pageshow', function() {
  var url_base = "http://candyman.biz" 
    , table$ = $('#mtable')   
    , tbody$ = $('#tbody');



    // get json data from server
	$.ajax({
        url: url_base +'/api/file_management/upload/' + localStorage.getItem('current_course_id'),
        type: 'GET',
        dataType: 'json',
        error: function(xhr, status, error) {
            alert(status);
            alert(xhr.responseText);
        },

        success: function(results) { 
            console.log(results);
            
            /* clear old results, then display the new results */
            //populate the files info from the json file
            if (results.length > 0) {
				  	//add titles to table heading$
                    // Title for column 1
                    console.log("inside the table");

                  for(var i = 0; i < results.length; i++)
                  {
                     	//populate title of file, timestamp, file type, file size and download link
                     	tbody$.append('<tr><td><strong>File: </strong>'+results[i].filename+'</td>'+'<td><strong>Date:</strong> '
                        +results[i].pub_date+'</td>'+'<td><strong>Type: </strong>'+results[i].filetype.split('/').pop()
                        +'</td>'+'<td><strong>Size: </strong> '+results[i].filesize+'B</td>'+'<td><button type="button" onclick=location.href="'+url_base+'/media/'
                        +results[i].file+'">Download</button></td></tr>');

                  }
              }
        }
      });
});
